#include "mpi.h"
#include<stdio.h>
#include<math.h>
#include <string.h>
#include<stdlib.h>

#define MAXSIZE 1000

int main(int argc, char *argv[]) {

    int myid, numprocs;
    int data[MAXSIZE], i,x,low,high,myresult = 0, result;
    char fn[255];
    FILE *fp;
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);

    //Process 0 gets the data from file and puts it into data variable
    if(myid == 0) {
        //strcpy(fn,getenv("./"));
        strcat(fn,"rand_data.txt");
        if((fp = fopen(fn,"r")) == NULL) {
            printf("Can't open input file");
            exit(1);
        } else printf("File opened");
        for(i = 0; i < MAXSIZE; i++) {
            fscanf(fp,"%d", &data[i]);
        }   
    }

    //Send data to all process from process 0
    MPI_Bcast(data, MAXSIZE, MPI_INT,0, MPI_COMM_WORLD);
    x = MAXSIZE / numprocs;
    low = myid * x;
    high = low + x;

    for (i = low; i < high; i++) {
        myresult += data[i];
    }

    printf("I got %d from %d\n", myresult, myid);
    MPI_Reduce(&myresult, &result, 1, MPI_INT, MPI_SUM,0,MPI_COMM_WORLD);
    if (myid == 0) printf("the sum is %d\n",result);
    MPI_Finalize();

return 0;

}
